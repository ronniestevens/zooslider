<?php
/*
    Plugin Name: ZOO Slider
    Description: Simple implementation of a bootstrap slideshow into WordPress
    Author: Ronnie Stevens
    Plugin URI: https://github.com/ronniestevens/zooslider
    Version: 1.0
*/

function zs_init() {	
    add_shortcode('zs-shortcode', 'zs_function');
    $args = array(
        'public' => true,
        'label' => 'Zoo Slides',
        'supports' => array(
            'title',
            'thumbnail',
        )
        );
        register_post_type('zoo_slides', $args);
        add_image_size('zs_widget', 180, 100, true);
        add_image_size('zs_function', 600, 280, true);
}
add_action('init', 'zs_init');


function zs_register_styles() {
    // register
    wp_register_style('zs_styles', plugins_url('zooslider/zooslider.css', __FILE__));
    // enqueue
    wp_enqueue_style('zs_styles');
}
add_action('wp_print_styles', 'zs_register_styles');


function zs_function($type='zs_function') {
    $args = array(
        'post_type' => 'zoo_slides',
        'posts_per_page' => 5
    );
    $result = '<div class="slider-wrapper theme-default">';
    $result .= '<div id="slider" class="nivoSlider">';
 
    //the loop
    $loop = new WP_Query($args);
    while ($loop->have_posts()) {
        $loop->the_post();
 
        $the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $type);
        $result .='<img title="'.get_the_title().'" src="' . $the_url[0] . '" data-thumb="' . $the_url[0] . '" alt=""/>';
    }
    $result .= '</div>';
    $result .='<div id = "htmlcaption" class = "nivo-html-caption">';
    $result .='<strong>This</strong> is an example of a <em>HTML</em> caption with <a href = "#">a link</a>.';
    $result .='</div>';
    $result .='</div>';
    return $result;
}

function zs_widgets_init() {
    register_widget('zs_Widget');
}
 
add_action('widgets_init', 'zs_widgets_init');

class zs_Widget extends WP_Widget {
 
    public function __construct() {
        parent::__construct('zs_Widget', 'Zoo Slideshow', array('description' => __('A Zoo Slideshow Widget', 'text_domain')));
    }

    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        }
        else {
            $title = __('Widget Slideshow', 'text_domain');
        }
        ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
        <?php
    }
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = strip_tags($new_instance['title']);
     
        return $instance;
    }
    public function widget($args, $instance) {
        extract($args);
        // the title
        $title = apply_filters('widget_title', $instance['title']);
        echo $before_widget;
        if (!empty($title))
            echo $before_title . $title . $after_title;
        echo zs_function('zs_widget');
        echo $after_widget;
    }
}

?>